package io.grainchain.testgooglemaps

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import io.grainchain.testgooglemaps.data.DetailTravelViewModel
import io.grainchain.testgooglemaps.data.model.Travel

class DatailWay : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var detailTravelViewModel: DetailTravelViewModel
    private var polylineOptions: PolylineOptions? = null
    private lateinit var travel: Travel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datail_way)
        polylineOptions = PolylineOptions().width(5f).color(Color.BLUE).geodesic(true)
        val idTravel = intent.extras?.getInt("idTravel")
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        detailTravelViewModel = ViewModelProviders.of(this).get(DetailTravelViewModel::class.java)
        idTravel?.let {
            detailTravelViewModel.getTravel(it).observe(this, Observer { travel ->
                if (travel != null) {
                    var txt: Spanned = Html.fromHtml("${resources.getString(R.string.name_way)} <b>${travel.name}</b>")
                    findViewById<TextView>(R.id.tvNameWay).text = txt
                    txt = Html.fromHtml("${resources.getString(R.string.kilometers)} <b>${travel.distance}</b>")
                    findViewById<TextView>(R.id.tvKilometer).text = txt
                    txt = Html.fromHtml("${resources.getString(R.string.tiempo)} <b>${travel.time}</b>")
                    findViewById<TextView>(R.id.tvTime).text = txt

                    detailTravelViewModel.getWayList(travel.id).observe(this, Observer { wayList ->
                        travel.wayList.addAll(wayList)
                        this.travel = travel
                        travel.wayList.forEach {way ->
                            polylineOptions?.add(LatLng(way.latitud, way.longitud))
                        }
                        mMap.addMarker(MarkerOptions().position(LatLng(travel.wayList[0].latitud, travel.wayList[0].longitud)))
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(travel.wayList[0].latitud, travel.wayList[0].longitud), 18f))

                        mMap.addPolyline(polylineOptions)

                        mMap.addMarker(MarkerOptions().position(LatLng(travel.wayList[(travel.wayList.size - 1)].latitud, travel.wayList[(travel.wayList.size - 1)].longitud)))
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(travel.wayList[(travel.wayList.size - 1)].latitud, travel.wayList[(travel.wayList.size - 1)].longitud), 18f))
                    })
                }
            })
        }
        findViewById<Button>(R.id.btnDelete).setOnClickListener {
            detailTravelViewModel.deleteTravel(travel)
            onBackPressed()
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        p0?.let {
            mMap = it
            mMap.uiSettings.isZoomControlsEnabled = true
        }
    }
}
