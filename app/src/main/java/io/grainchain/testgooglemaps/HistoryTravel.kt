package io.grainchain.testgooglemaps

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.grainchain.testgooglemaps.data.HistoryViewModel
import io.grainchain.testgooglemaps.data.model.Travel
import io.grainchain.testgooglemaps.holderadapter.HistoryHolder

class HistoryTravel : Fragment() {

    private lateinit var historyAdapter: HistoryHolder
    private var travelList: ArrayList<Travel> = ArrayList()
    private lateinit var historyViewModel: HistoryViewModel
    private lateinit var recycler: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_history_travel, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler = view.findViewById(R.id.recycler)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(activity)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel::class.java)
        historyAdapter = HistoryHolder(travelList, historyViewModel)
        recycler.adapter = historyAdapter

        historyAdapter.setOnItemClickListener(object: HistoryHolder.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                startActivity(Intent(context, DatailWay::class.java).putExtra("idTravel", travelList[position].id))
            }
        })
        historyViewModel.allTravel.observe(this, Observer { t ->
            travelList.clear()
            historyAdapter.notifyDataSetChanged()
            travelList.addAll(t)
            historyAdapter.notifyDataSetChanged()
        })
    }
}
