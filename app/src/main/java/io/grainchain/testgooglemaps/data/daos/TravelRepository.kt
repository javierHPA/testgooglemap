package io.grainchain.testgooglemaps.data.daos

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import io.grainchain.testgooglemaps.data.model.Travel
import io.grainchain.testgooglemaps.data.model.Way

class TravelRepository(private val travelDAO: TravelDAO) {

    @WorkerThread
    fun insetTravel(travel: Travel): Long = travelDAO.inseertTravel(travel)

    @WorkerThread
    fun insertWay(wayList: ArrayList<Way>) {
        travelDAO.insertWay(wayList)
    }

    val allTravel: LiveData<List<Travel>> = travelDAO.getAllTravel()

    @WorkerThread
    fun deleteTravel(travel: Travel): Int = travelDAO.deleteTravel(travel)

    @WorkerThread
    fun deleteWay(idTravel: Int) = travelDAO.deleteWay(idTravel)

    @WorkerThread
    fun getTravel(idTravel: Int) = travelDAO.getTravel(idTravel)

    @WorkerThread
    fun getWayList(idTravel: Int) = travelDAO.getWayList(idTravel)
}