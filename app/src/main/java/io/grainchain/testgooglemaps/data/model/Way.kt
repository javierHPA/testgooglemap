package io.grainchain.testgooglemaps.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(
        entity = Travel::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("idTravel")
    )]
)
data class Way(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id", index = true) var id: Int,
               @ColumnInfo(name = "idTravel", index = true) var idTravel: Int,
               @ColumnInfo(name = "latitud") var latitud: Double,
               @ColumnInfo(name = "longitud") var longitud: Double) {

    constructor(): this(0, 0,0.0, 0.0)
}