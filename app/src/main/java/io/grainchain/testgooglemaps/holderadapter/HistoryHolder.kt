package io.grainchain.testgooglemaps.holderadapter

import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.grainchain.testgooglemaps.R
import io.grainchain.testgooglemaps.data.HistoryViewModel
import io.grainchain.testgooglemaps.data.model.Travel

class HistoryHolder(private val travelList: ArrayList<Travel>, private val historyViewModel: HistoryViewModel): RecyclerView.Adapter<HistoryHolder.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_recycler_history, parent, false))

    override fun getItemCount(): Int = travelList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val travel = travelList[position]
        var txt: Spanned = Html.fromHtml("${holder.itemView.context.resources.getString(R.string.name_way)} <b>${travel.name}</b>")
        holder.tvNameWay.text = txt
        txt = Html.fromHtml("${holder.itemView.context.resources.getString(R.string.kilometers)} <b>${travel.distance}</b>")
        holder.tvKilometer.text = txt
        holder.tvDeleteWay.setOnClickListener {
            historyViewModel.deleteTravel(travelList[holder.adapterPosition])
                //Toast.makeText(holder.itemView.context, "Se elimino correctamente", Toast.LENGTH_LONG).show()
            //else Toast.makeText(holder.itemView.context, "Ocurrio un error", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        private lateinit var onItemClickListener: OnItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        HistoryHolder.onItemClickListener = onItemClickListener
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvNameWay = itemView.findViewById<TextView>(R.id.tvNameWay)
        var tvKilometer = itemView.findViewById<TextView>(R.id.tvKilometer)
        var tvDeleteWay = itemView.findViewById<TextView>(R.id.tvDelete)
        init {
            itemView.setOnClickListener{view ->
                onItemClickListener.onItemClick(view, adapterPosition)
            }
        }
    }
}