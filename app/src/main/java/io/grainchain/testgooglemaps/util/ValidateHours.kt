package io.grainchain.testgooglemaps.util

import java.util.*
import kotlin.math.abs

class ValidateHours {
    companion object {
        fun getHour(): String {
            val calendar = GregorianCalendar()
            val hora: String
            if (calendar.get(Calendar.HOUR_OF_DAY) < 10)
                if (calendar.get(Calendar.MINUTE) < 10)
                    hora = "0" + calendar.get(Calendar.HOUR_OF_DAY) + ":0" + calendar.get(Calendar.MINUTE)
                else hora = "0" + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE)
            else if (calendar.get(Calendar.MINUTE) < 10)
                hora = calendar.get(Calendar.HOUR_OF_DAY).toString() + ":0" + calendar.get(Calendar.MINUTE)
            else hora = calendar.get(Calendar.HOUR_OF_DAY).toString() + ":" + calendar.get(Calendar.MINUTE)
            return hora
        }

        fun getDiferenceHour(startTime: String, endTime: String): String {
            val h1 = startTime.split(":")
            val h2 = endTime.split(":")
            val calendarStar: Calendar = GregorianCalendar.getInstance()
            calendarStar.set(Calendar.HOUR, h1[0].toInt())
            calendarStar.set(Calendar.MINUTE, h1[1].toInt())

            val calendarEnd: Calendar = GregorianCalendar.getInstance()
            calendarEnd.set(Calendar.HOUR, h2[0].toInt())
            calendarEnd.set(Calendar.MINUTE, h2[1].toInt())

            val diff: Long = calendarStar.time.time - calendarEnd.time.time
            return "${abs((diff/(1000*60*60))%24)}:${abs((diff/(1000*60))%60)}"
        }
    }
}